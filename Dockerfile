# Use an official Python 3.7 runtime with slim Stretch Debian
FROM python:3.7-slim-stretch

# Install required packages
RUN pip3 install aiohttp

# Set the working directory to /nametag
WORKDIR /nametag

# Copy the required files to /nametag
COPY LICENSE README.md nametag_elg_rest_server.py /nametag/

# Expose the REST server port
EXPOSE 8080

# Run the server when the container launches
CMD ["/usr/bin/env", "python3", "/nametag/nametag_elg_rest_server.py", "--port=8080", "--max_request_size=1024000", "--nametag_host=localhost", "--nametag_port=8002"]
